# Release information about Scriptaculous

version = "1.8.2"

description = "Scriptaculous packaged as TurboGears widgets."
author = "Thomas Fuchs, Sam Stephenson, Kevin Dangoor, Fred Lin, W-Mark Kubacki"
email = "mark@ossdl.de"
copyright = "Copyright 2007-2009"

url = "http://tgwidgets.ossdl.de/"
download_url = "http://static.ossdl.de/tgwidgets/downloads/"
license = "MIT"
