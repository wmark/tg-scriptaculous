Scriptaculous
-------------

This is a packaging of the Scriptaculous and Prototype JavaScript libraries as TurboGears widgets. Scriptaculous was originally written by Thomas Fuchs and Prototype was originally written by Sam Stephenson.

http://script.aculo.us
