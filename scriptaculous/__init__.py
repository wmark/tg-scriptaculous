from scriptaculous.widgets import prototype, scriptaculous, scriptaculous_js,\
                                  prototype_js, builder_js, controls_js,\
                                  dragdrop_js, effects_js, slider_js,\
                                  unittest_js

__all__ = ["scriptaculous","prototype",
           "prototype_js", "scriptaculous_js",
           "builder_js", "controls_js",
           "dragdrop_js", "effects_js",
           "slider_js", "unittest_js",
           "sound_js"
           ]
